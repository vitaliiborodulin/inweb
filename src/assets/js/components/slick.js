var portfolio = $('#portfolio');

portfolio.slick({
	arrows: false,
	dots: true,
	speed: 800,
	slidesToShow: 2,
	slidesToScroll: 2,
	responsive: [
    {
      breakpoint: 575,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false
      }
    }
  ]
});

$('#portfolio-arrow-prev').on('click', function(e) {
	e.preventDefault();

	portfolio.slick('slickPrev');
});

$('#portfolio-arrow-next').on('click', function(e) {
	e.preventDefault();

	portfolio.slick('slickNext');
});